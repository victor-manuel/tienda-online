import Vue from "vue";
import Router from "vue-router";
import Vuetify from "vuetify";

Vue.use(Router);
Vue.use(Vuetify);

export default new Router({
    routes: [
        {
            path: "/",
            name: "index",
            component: require("./views/index.vue").default
        },
        {
            path: "/table",
            name: "table",
            component: require("./views/dataTableExplame.vue").default
        },
        {
            path: "/created",
            name: "created",
            component: require("./views/admin/created.vue").default
        }
    ],
    mode: "history"
});
