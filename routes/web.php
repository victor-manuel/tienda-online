<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@index')->name('index');


//Auth::routes();

Route::get('/category', 'CategoryController@index')->name('category-index');
// Route::get('admin/category/create/', 'CategoryController@create')->name('category-create');
// Route::post('admin/category/store', ['as'=>'category.store', 'uses'=>'CategoryController@store']);
// Route::get('admin/category/update/{category}/', 'CategoryController@update')->name('category-update');
// Route::post('admin/category/update/{category}/', 'CategoryController@update')->name('category-update');
// Route::get('admin/category/delete/{category}/', 'CategoryController@delete')->name('category-delete');



Route::get('/product', 'ProductController@index')->name('products-index');
Route::post('/product-create', 'ProductController@store')->name('products-create');
// Route::get('admin/blog/update/{post}/', 'ProductController@update')->name('blog-update');
// Route::post('admin/blog/update/{post}/', 'ProductController@update')->name('blog-update');
// Route::get('admin/blog/delete/{post}/', 'ProductController@delete')->name('blog-delete');
// Route::post('admin/blog/store', ['as'=>'blog.store', 'uses'=>'ProductController@store']);
// Route::get('admin/', 'AdminController@index');


