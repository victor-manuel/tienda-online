<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title', 'imagen', 'description', 'precing', 'category_id'];
    // protected $fillable = ['id', 'title', 'imagen', 'description', 'pricing', 'category_id' ];
    // public function category(){
    //     return $this->belongsTo(Category::class);
    // }
}
